# NextFilm

NextFilm is a recommendation system, which suggests new films to watch, based on the collaborative filtering. All the user have to do is enter his favourite movie or the movie you have last seen. Application will recommend the list of movies he should see for the next time. 

More information on [Wiki](https://bitbucket.org/david_pavel/nextfilm/wiki/Home).

## Installation instructions

### Build application
Use ```mvn package``` command to build the application.
### Database selection
Extract one of database archives form ```./databse/*``` to directory ```./nextFilm.embedded.db/```.
If ```./nextFilm.embedded.db/``` directory is empty or not exist, applications creates empty database.
### Run application
Simply run packaged jar file.
```
java -jar ./target/nextfilm-0.0.1.jar
```
Application is running on http://localhost:8080/.