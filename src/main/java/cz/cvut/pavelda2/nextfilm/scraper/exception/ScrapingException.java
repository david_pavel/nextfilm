package cz.cvut.pavelda2.nextfilm.scraper.exception;

public class ScrapingException extends Exception {

	private static final long serialVersionUID = -4910464090574882843L;

	public ScrapingException() {
		super();
	}

	public ScrapingException(String message, Throwable cause) {
		super(message, cause);
	}

	public ScrapingException(String message) {
		super(message);
	}

	public ScrapingException(Throwable cause) {
		super(cause);
	}
	
}
