package cz.cvut.pavelda2.nextfilm.scraper.fdb;

import java.io.File;
import java.util.Set;

import cz.cvut.pavelda2.nextfilm.domain.Movie;
import cz.cvut.pavelda2.nextfilm.domain.Rating;
import cz.cvut.pavelda2.nextfilm.scraper.exception.ScrapingException;

public interface FdbScraper {
	public Movie retrieveMovie(Integer fdbId) throws ScrapingException;
	public Movie retrieveMovie(File file) throws ScrapingException;
	public Set<Rating> retireveRatings(File file) throws ScrapingException;
}
