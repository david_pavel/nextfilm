package cz.cvut.pavelda2.nextfilm.scraper.fdb;

import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cz.cvut.pavelda2.nextfilm.domain.Rating;
import cz.cvut.pavelda2.nextfilm.domain.User;

public class JSoupFdbScraperHelper {
	
	public static boolean hasContent(Document document) {
		return document.getElementById("zakladni_info") != null;
	}

	public static Integer extractRating(Document document) {
		Elements ratingElems = document
			.getElementById("zakladni_info")
			.getElementsByClass("hodnoceni");
		if (ratingElems.size() == 0) {
			return null;
		}
		String rating = ratingElems.first().getElementsByTag("a").first().text();
		return (int)(Double.parseDouble(rating.replace("%", "")) * 10);
	}

	public static String extractMovieName(Document document) {
		return document
				.getElementById("zakladni_info")
				.getElementsByTag("h1").first()
				.getElementsByTag("a").first()
				.text();
	}

	public static Long extractMovieId(Document document) {
		String url = document
			.getElementsByTag("head").first()
			.getElementsByAttributeValue("property", "og:url").first()
			.attr("content");
		int lastSlash = url.lastIndexOf("/");
		if (lastSlash == -1) {
			return null;
		}
		return Long.parseLong(url.substring(lastSlash+1));
		
	}

	public static List<String> extractCountries(Document document) {
		Elements rows = getInfoRows(document);
		for (Element row : rows) {
			String key = row.getElementsByClass("left_text").first().text();
			if (key.equals("Země:")) {
				Elements countries = row
						.getElementsByClass("right_text").first()
						.getElementsByTag("a");
				return countries.stream().map((c) -> (c.text())).collect(Collectors.toList());
			}
		}
		return Collections.emptyList();		
	}
	
	public static List<String> extractGenres(Document document) {
		Elements rows = getInfoRows(document);
		for (Element row : rows) {
			String key = row.getElementsByClass("left_text").first().text();
			if (key.equals("Žánr:")) {
				Elements genres = row
						.getElementsByClass("right_text").first()
						.getElementsByTag("a");
				return genres.stream().map((c) -> (c.text())).collect(Collectors.toList());
			}
		}
		return Collections.emptyList();		
	}
	
	public static Integer extractYear(Document document) {
		Elements rows = getInfoRows(document);
		for (Element row : rows) {
			String key = row.getElementsByClass("left_text").first().text();
			if (key.equals("Rok:")) {
				String year = row
						.getElementsByClass("right_text").first()
						.text();
				return Integer.parseInt(year);
			}
		}
		return null;		
	}
	
	public static Integer extractDuration(Document document) {
		Elements rows = getInfoRows(document);
		for (Element row : rows) {
			String key = row.getElementsByClass("left_text").first().text();
			if (key.equals("Délka:")) {
				String duration = row
						.getElementsByClass("right_text").first()
						.text();
				int firstSpace = duration.indexOf(32);
				return firstSpace == -1 ? null :  Integer.parseInt(duration.substring(0,firstSpace));
			}
		}
		return null;		
	}
	
	private static Elements getInfoRows(Document document) {
		return document
				.getElementById("zakladni_info")
				.getElementsByClass("infotext").first()
				.getElementsByClass("row");
	}

	public static Rating extractRatings(Element ratingElem) {
		// User
		Element userElem = ratingElem
			.getElementsByClass("cols").first()
			.getElementsByTag("a").first();		
		String username = userElem.text();		
		Matcher m = Pattern.compile(".*/clen/(\\d+)-.*").matcher(userElem.attr("href"));
		m.find();
		Integer userId = Integer.parseInt(m.group(1));
		
		String mark = ratingElem
			.getElementsByClass("hodnoceni").first()
			.text().replace("(","").replace(")","").trim();
		
		User user = new User(username);
		user.setUserId(userId);
		
		return new Rating(user, null, Integer.parseInt(mark));
	}

}
