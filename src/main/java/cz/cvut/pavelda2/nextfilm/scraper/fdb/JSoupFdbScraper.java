package cz.cvut.pavelda2.nextfilm.scraper.fdb;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cz.cvut.pavelda2.nextfilm.domain.Country;
import cz.cvut.pavelda2.nextfilm.domain.Genre;
import cz.cvut.pavelda2.nextfilm.domain.Movie;
import cz.cvut.pavelda2.nextfilm.domain.Rating;
import cz.cvut.pavelda2.nextfilm.scraper.exception.ScrapingException;

public class JSoupFdbScraper implements FdbScraper {

	private static final String FDB_HOME_URL = "http://www.fdb.cz/";
	private static final String FILM_INFO_URL = FDB_HOME_URL + "movie/XXX/hodnoceni/";


	private Movie retrieveMovie(Document document) {
		if (!JSoupFdbScraperHelper.hasContent(document)) {
			return null;
		}
		Integer rating = JSoupFdbScraperHelper.extractRating(document);
		Long movieId = JSoupFdbScraperHelper.extractMovieId(document);
		String movieName = JSoupFdbScraperHelper.extractMovieName(document);
				
		List<String> countries = JSoupFdbScraperHelper.extractCountries(document);
		Integer year = JSoupFdbScraperHelper.extractYear(document);
		Integer duration = JSoupFdbScraperHelper.extractDuration(document);
		List<String> genres = JSoupFdbScraperHelper.extractGenres(document);

		Movie movie = new Movie(movieId,movieName);
		movie.setCountries(countries.stream().map(Country::new).collect(Collectors.toSet()));
		movie.setFdbRating(rating);
		movie.setDuration(duration);
		movie.setYear(year);
		movie.setGenres(genres.stream().map(Genre::new).collect(Collectors.toSet()));		
		return movie;
	}

	@Override
	public Movie retrieveMovie(Integer fdbId) throws ScrapingException {
		try {
			Document document = Jsoup.connect(FILM_INFO_URL + fdbId).get();
			return retrieveMovie(document);
		} catch (HttpStatusException ex) {
			throw new ScrapingException(MessageFormat.format("Movie {0} not found.", fdbId), ex);
		} catch (Exception ex) {
			throw new ScrapingException(MessageFormat.format("Can not retrieve movie {0}.", fdbId), ex);
		}
	}

	@Override
	public Movie retrieveMovie(File file) throws ScrapingException {
		try {
			Document document = Jsoup.parse(file,"UTF-8", FDB_HOME_URL);
			return retrieveMovie(document);
		} catch (IOException ex) {
			throw new ScrapingException(MessageFormat.format("Can not retrieve movie from file {0}.",
					file.toString()), ex);
		}
	}

	private Set<Rating> retrieveRatings(Document document) {
		if (!JSoupFdbScraperHelper.hasContent(document)) {
			return null;
		}
		
		Set<Rating> ratings = new HashSet<Rating>();
		Elements ratingElems = document.getElementsByClass("hodno_data");
		for (Element ratingElem : ratingElems) {
			Rating rating = JSoupFdbScraperHelper.extractRatings(ratingElem);
			if (rating != null) {
				ratings.add(rating);
			}
		}
		return ratings;
	}

	@Override
	public Set<Rating> retireveRatings(File file) throws ScrapingException {
		try {
			Document document = Jsoup.parse(file,"UTF-8", FDB_HOME_URL);
			return retrieveRatings(document);
		} catch (IOException ex) {
			throw new ScrapingException(MessageFormat.format("Can not retrieve movie from file {0}.",
					file.toString()), ex);
		}
	}
	
	
	
	
}
