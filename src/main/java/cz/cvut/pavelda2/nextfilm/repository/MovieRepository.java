package cz.cvut.pavelda2.nextfilm.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import cz.cvut.pavelda2.nextfilm.domain.Movie;
import cz.cvut.pavelda2.nextfilm.domain.MovieRecommendation;

@RepositoryRestResource(collectionResourceRel = "movies", path = "movies")
public interface MovieRepository extends GraphRepository<Movie> {

	@Query("match (m:Movie:_Movie) return m order by m.title")
	List<Movie> findAllMovies();

	@Query("match (m:Movie:_Movie) where m.fdbId={0} return m")
	Movie findById(Long id);

	@Query(value="match (S:Movie:_Movie) where S.title=~{0} return S order by S.title",
		   countQuery="match (S:Movie:_Movie) where S.title=~{0} return count(S)") 
	Page<Movie> findByTitleQuery(String query, Pageable page);	

	@Query(value = "match (m:Movie:_Movie)-->(g:Genre:_Genre) where g.name=~{0} return m order by m.title")
	Page<Movie> findByGenreQuery(String genre, Pageable pageable);


	@Query(value = "match (m:Movie:_Movie)-->(g:Country:_Country) where g.name=~{0} return m order by m.title")
	Page<Movie> findByCountry(String query, Pageable pageable);

	@Query(value =
			"match (my:Movie)<-[myRating]-(u:User)-[mRating]->(m:Movie)" +
			" where my.fdbId={0} AND myRating.stars >= {1}" +
			" return m as movie, count(m) as count, avg(mRating.stars) as avg, avg(mRating.stars) * (1+(count(m)/100.0))  as normAvg" +
			" order by normAvg DESC, avg ",
		countQuery="match (my:Movie)<-[myRating]-(u:User)-[mRating]->(m:Movie)" +
			" where my.fdbId={0} AND myRating.stars >= {1}" +
			" return count(distinct m)")
	Page<MovieRecommendation> recommendByMovie(Long favouriteMovieId, Integer minRating, Pageable pageable);

	@Query(value =
			"match (S:Movie:_Movie)<-[myRating]-(u:User:_User)-[mRating]->(m:Movie:_Movie)" +
			" where S.fdbId={0} AND myRating.stars >= {1}" +
			" return m as movie, count(m) as count, avg(mRating.stars) as avg, avg(mRating.stars) * (1+(count(m)/100.0)) as normAvg" +
			" order by normAvg DESC, avg" +
			" limit {2}" /*,
		countQuery="match (my:Movie:_Movie)<-[myRating]-(u:User:_User)-[mRating]->(m:Movie:_Movie)" +
			" where my.fdbId={0} AND myRating.stars >= {1}" +
			" return count(distinct m)"*/)
	List<MovieRecommendation> recommendByFavouriteMovie(Long favouriteMovieId, int minRating, int maxResults);

	@Query(value =
			"match (S:Movie:_Movie)<-[SR:RATED_BY]-(U:User:_User)-[R:RATED_BY]->(M:Movie:_Movie)" +
			" where S.fdbId={0} AND abs(SR.stars - {1}) <= 1" +
			" return M as movie, count(M) as count, avg(R.stars) as avg, avg(R.stars)*(1+(count(M)/100.0)) as normAvg" +
			" order by normAvg DESC, avg" +
			" limit {2}" /*,
		countQuery="match (S:Movie)<-[SR:RATED_BY]-(U:User)-[R:RATED_BY]->(M:Movie)" +
			" where my.fdbId={0} AND abs(SR.stars - {1}) <=2" +
			" return count(distinct M)" */)
	List<MovieRecommendation> recommendByMovieRating(Long favouriteMovieId, Integer rating, int maxResults);

	@Query(value = "match (m:Movie:_Movie)<--(u:User:_User) where u.username=~{0} return m")
	List<Movie> findAllByUserQuery(String query);

	
}