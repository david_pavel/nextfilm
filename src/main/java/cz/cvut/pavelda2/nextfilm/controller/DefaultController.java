package cz.cvut.pavelda2.nextfilm.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DefaultController {
	final private Logger logger = LoggerFactory.getLogger(getClass());
	
	@RequestMapping("/")
	public String root() {
		return "redirect:/Movie/Search";
	}
	
	

	@RequestMapping(value="/Error")
	public String error(HttpServletRequest request, Model model) {	
		
		Throwable exception = (Throwable) request.getAttribute("javax.servlet.error.exception");
		String uri = (String) request.getAttribute("javax.servlet.error.request_uri");
		Integer status = (Integer) request.getAttribute("javax.servlet.error.status_code");
			
		logger.info("Processing error: [uri=" + uri + ", status=" + status + "]", exception);

		model.addAttribute("status",status);
		model.addAttribute("uri",uri);
		
		return "/error";
	}
	
}
