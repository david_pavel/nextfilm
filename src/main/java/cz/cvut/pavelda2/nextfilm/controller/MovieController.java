package cz.cvut.pavelda2.nextfilm.controller;


import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cz.cvut.pavelda2.nextfilm.domain.Movie;
import cz.cvut.pavelda2.nextfilm.domain.MovieRecommendation;
import cz.cvut.pavelda2.nextfilm.repository.MovieRepository;
import cz.cvut.pavelda2.nextfilm.scraper.exception.ScrapingException;
import cz.cvut.pavelda2.nextfilm.service.MovieImportService;
import cz.cvut.pavelda2.nextfilm.service.MovieService;

@Controller
@RequestMapping("/Movie")
public class MovieController {

	@Autowired
	private MovieService service;
	
	@Autowired
	private MovieRepository toBeReplacedMovieRepository;

	@Autowired
	private MovieImportService importService;
	
	@RequestMapping("/")
	public String root() {
		return "redirect:/Search";
	}
	
	@RequestMapping(value="/Search", method=RequestMethod.GET)
	public String search(
			@RequestParam(required=false,value="q") String title,
			@RequestParam(required=false,value="genre") String genre,
			@RequestParam(required=false,value="country") String country,
			@RequestParam(required=false, value="page", defaultValue="0") int page,
			@RequestParam(required=false,value="size", defaultValue="20") int size,
			Model model) {
		
		if (!StringUtils.isEmpty(title)) {		
			Page<Movie> moviesPage = service.findByTitleLike(title, new PageRequest(page, size));
			model.addAttribute("moviesPage", moviesPage);
			model.addAttribute("movies", moviesPage.getContent());
		} else if (!StringUtils.isEmpty(genre)) {
			Page<Movie> moviesPage = service.findByGenre(genre, new PageRequest(page, size));
			model.addAttribute("moviesPage", moviesPage);
			model.addAttribute("movies", moviesPage.getContent());
		} else if (!StringUtils.isEmpty(country)) {
			Page<Movie> moviesPage = service.findByCountry(country, new PageRequest(page, size));
			model.addAttribute("moviesPage", moviesPage);
			model.addAttribute("movies", moviesPage.getContent());
		}
		return "/search";
	}

	@RequestMapping(value="/List", method=RequestMethod.GET)
	public String detail(Model model, @RequestParam(required=false,value="startWith") String startWith) {
		if (startWith != null) {
			List<Movie> movies = service.findByTitleStart(startWith, new PageRequest(0, 100)).getContent();
			model.addAttribute("movies", movies);
		}
		return "/movie/list";
	}
	
	@RequestMapping(value="/{fdbId}", method=RequestMethod.GET)
	public String detail(@PathVariable("fdbId") Long fdbId, Model model) {
		System.out.println("FDB ID: " + fdbId);
		Movie movie = service.findById(fdbId);
		if (movie == null) {
			throw new ResourceNotFoundException("Movie with id " + fdbId + " not found.");
		}		
		model.addAttribute("movie", movie);
		return "/movie/detail";
	}

	@RequestMapping(value="/Scrape", method=RequestMethod.POST)
	public String scrape(@RequestParam("fdbId") String fdbIdFormat) throws ScrapingException {
		System.out.println("Scraping movie by id: " + fdbIdFormat);		
		Matcher matcher = Pattern.compile("^[\\s+]*(\\d+)([\\s+]*-[\\s+]*)?(\\d+)[\\s+]*$").matcher(fdbIdFormat);
		if (matcher.matches() && matcher.groupCount() >= 1 && matcher.groupCount() <= 3) {
			if (matcher.groupCount() == 1) {
				System.out.println("ONE");
				Long fdbId = Long.parseLong(matcher.group(1));
				importService.importById(fdbId);
				return "redirect:/Monitor/Scraping";
			} else {
				System.out.println("RANGE");
				Long fdbIdStart = Long.parseLong(matcher.group(1));
				Long fdbIdEnd = Long.parseLong(matcher.group(3));
				importService.importByIdRange(fdbIdStart,fdbIdEnd);
				return "redirect:/Monitor/Scraping";
			}
		} else {
			throw new ResourceNotFoundException("Wrong id format: " + fdbIdFormat);
		}
	}
	
	@RequestMapping(value="/{fdbId}/Recommend", method=RequestMethod.GET)
	public String recommendations(@PathVariable("fdbId") Long fdbId,
			@RequestParam(value="limit",defaultValue="10",required=false) Integer limit, Model model) {
		List<MovieRecommendation> recommendations = service.recommendByFavouriteMovie(fdbId, limit);
		model.addAttribute("recommendations", recommendations);
		return "/movie/detail :: recommendations";
	}
	
	@RequestMapping(value="/{fdbId}/RecommendByRating", method=RequestMethod.GET)
	public String recommendationsByRating(@PathVariable("fdbId") Long fdbId,
			@RequestParam(value="limit",defaultValue="10",required=false) Integer limit, 
			@RequestParam("rating") Integer rating,
			Model model) {
		
		List<MovieRecommendation> recommendations = service.recommendByMovieRating(fdbId,rating, limit);
		model.addAttribute("recommendationsByRating", recommendations);
		return "/movie/detail :: recommendations-by-rating";
	}	
	
}
