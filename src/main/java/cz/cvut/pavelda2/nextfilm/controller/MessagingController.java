package cz.cvut.pavelda2.nextfilm.controller;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cz.cvut.pavelda2.nextfilm.domain.Movie;
import cz.cvut.pavelda2.nextfilm.service.MovieService;
import cz.cvut.pavelda2.nextfilm.websocket.MovieRequest;
import cz.cvut.pavelda2.nextfilm.websocket.MovieWSResult;
import cz.cvut.pavelda2.nextfilm.websocket.MovieWSResult.Status;


@Controller
public class MessagingController {
	
	@Autowired
	private MovieService service;
	
	private Random rand = new Random();
	
	 @MessageMapping("/monitor/event/save")
	 @SendTo("/monitor/scraping")
	 public MovieWSResult monitorScrapeProcess(MovieRequest request) {
		 if (rand.nextBoolean()) {
			 return new MovieWSResult(Status.STARTED, request.getId());
		 } else {
			 Movie movie = service.findById(request.getId());
			 if (movie != null) {
				 return new MovieWSResult(Status.COMPLETED, request.getId(), movie.getTitle(), movie.getFdbRating());
			 } else {
				 return new MovieWSResult(Status.EXCEPTION, request.getId());
			 }
		 }
	 }
	 
	 @RequestMapping("/Monitor/Scraping")
	 public String getMonitor(){
		 	return "/monitor/movie-scrape";
	 }
}
