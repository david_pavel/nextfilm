package cz.cvut.pavelda2.nextfilm.service;

import cz.cvut.pavelda2.nextfilm.domain.Movie;

public interface MovieImportServiceEventHandler {
	public void onStart(Long movieId);
	public void onComplete(Long movieId, Movie movie);
	public void onException(Long movieId, Throwable throwable);
}
