package cz.cvut.pavelda2.nextfilm.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;

import cz.cvut.pavelda2.nextfilm.domain.Movie;
import cz.cvut.pavelda2.nextfilm.domain.Rating;
import cz.cvut.pavelda2.nextfilm.scraper.exception.ScrapingException;
import cz.cvut.pavelda2.nextfilm.scraper.fdb.FdbScraper;

public class MovieImportServiceImpl implements MovieImportService {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private static final String DEFAULT_SCRAPED_MOVIES_PATH = "c:\\Users\\pavelda2\\Workspace\\CVUT\\FIT\\magister\\semestr_2\\MI-DDW\\Homeworks\\hw2\\fdb\\data\\";
	private String scrapedMoviesPath = DEFAULT_SCRAPED_MOVIES_PATH;
	
	private MovieService service;
	private FdbScraper scraper;
	
	private List<MovieImportServiceEventHandler> eventHandlers = new ArrayList<>();
	
	public void registerEventHandler(MovieImportServiceEventHandler handler) {
		eventHandlers.add(handler);
	}
	public boolean unregisterEventHandler(MovieImportServiceEventHandler handler) {
		return eventHandlers.remove(handler);
	}
	
	public void setScrapedMoviesPath(String scrapedMoviesPath) {
		this.scrapedMoviesPath = scrapedMoviesPath;
	}

	@Autowired
	public void setService(MovieService service) {
		this.service = service;
	}

	@Autowired
	public void setScraper(FdbScraper scraper) {
		this.scraper = scraper;
	}

	@Override
	@Async
	public void importById(Long fdbId) throws ScrapingException {
		logger.debug("Importing movie with id {}", fdbId);
		fireScrapingStartedEvent(fdbId);
		try {
			File file = new File(scrapedMoviesPath,fdbId + ".html");
			Movie retrievedMovie = scraper.retrieveMovie(file);
			Set<Rating> retrievedRatings = scraper.retireveRatings(file);
			service.save(retrievedMovie, retrievedRatings);
			
			retrievedMovie.setRatings(retrievedRatings);
			fireScrapingCompleteEvent(fdbId, retrievedMovie);
		} catch (Exception ex) {
			fireScrapingExceptionEvent(fdbId, ex);
		}
	}
	
	@Override
	@Async
	public void importByIdRange(Long fdbIdStart, Long fdbIdEnd) {
		logger.debug("Importing movies in id range {} - {}", fdbIdStart,fdbIdEnd);
		assert fdbIdStart <= fdbIdEnd;
		
		List<Long> fails = new ArrayList<>();
		
		for (long fdbId = fdbIdStart; fdbId < fdbIdEnd; fdbId++) {
			try {
				fireScrapingStartedEvent(fdbId);
				File file = new File(scrapedMoviesPath,fdbId + ".html");
				Movie retrievedMovie;
				retrievedMovie = scraper.retrieveMovie(file);
				if (retrievedMovie == null) {
					throw new ScrapingException("No film found by id " + fdbId);
				}
				Set<Rating> retrievedRatings = scraper.retireveRatings(file);
				service.save(retrievedMovie, retrievedRatings);
				
				retrievedMovie.setRatings(retrievedRatings);
				fireScrapingCompleteEvent(fdbId, retrievedMovie);
			} catch (ScrapingException ex) {
				fails.add(fdbId);
				fireScrapingExceptionEvent(fdbId, ex);
			}
		}
		
		if (!fails.isEmpty()) {
			throw new RuntimeException("Following movies were not scraped: " + fails.toString());
		}
		
	}

	/* ========================== */
	/* === Events =============== */
	/* ========================== */

	private void fireScrapingExceptionEvent(Long movieId, Throwable ex) {
		eventHandlers.forEach((h) -> h.onException(movieId, ex));		
	}
	private void fireScrapingStartedEvent(Long movieId) {
		eventHandlers.forEach((h) -> h.onStart(movieId));	
	}
	private void fireScrapingCompleteEvent(Long movieId, Movie movie) {
		eventHandlers.forEach((h) -> h.onComplete(movieId, movie));		
	}
	
}
