package cz.cvut.pavelda2.nextfilm.service;



public interface DatabaseInfoService {

	public Long moviesCount();
	public Long ratedMoviesCount();
	public Long moviesRatingsCount();
	public Long usersCount();
	public Long genresCount();
	public Long countriesCount();

}
