package cz.cvut.pavelda2.nextfilm.service;

import cz.cvut.pavelda2.nextfilm.scraper.exception.ScrapingException;

public interface MovieImportService {
	
	public void registerEventHandler(MovieImportServiceEventHandler handler);
	
	public boolean unregisterEventHandler(MovieImportServiceEventHandler handler);
	
	
	public void importById(Long fdbId) throws ScrapingException;

	public void importByIdRange(Long fdbIdStart, Long fdbIdEnd);
	
}
