package cz.cvut.pavelda2.nextfilm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.cvut.pavelda2.nextfilm.domain.Country;
import cz.cvut.pavelda2.nextfilm.domain.Genre;
import cz.cvut.pavelda2.nextfilm.domain.Movie;
import cz.cvut.pavelda2.nextfilm.domain.Rating;
import cz.cvut.pavelda2.nextfilm.domain.User;


@Service("databaseInfoService")
@Transactional(readOnly=true)
@Cacheable(value="dbInfo", key="#root.method.name")	
public class DatabaseInfoServiceImpl implements DatabaseInfoService {

	private Neo4jTemplate template;

	
	@Autowired
	public void setNeo4jTemplate(Neo4jTemplate template) {
		this.template = template;
	}


	@Override
	public Long moviesCount() {
		return template.count(Movie.class);
	}

	@Override
	public Long ratedMoviesCount() {
		return (Long)(template.query("match (m:Movie:_Movie)<--(u:User:_User) return count(distinct m)", null).single().values().iterator().next());
	}

	@Override
	public Long moviesRatingsCount() {
		return template.count(Rating.class);
	}

	@Override
	public Long usersCount() {
		return template.count(User.class);
	}

	@Override
	public Long genresCount() {
		return template.count(Genre.class);
	}

	@Override
	@Cacheable
	public Long countriesCount() {
		return template.count(Country.class);
	}
}
