package cz.cvut.pavelda2.nextfilm.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.cvut.pavelda2.nextfilm.domain.Movie;
import cz.cvut.pavelda2.nextfilm.domain.MovieRecommendation;
import cz.cvut.pavelda2.nextfilm.domain.Rating;
import cz.cvut.pavelda2.nextfilm.repository.MovieRepository;

@Service
@Transactional(readOnly=true)
public class MovieServiceImpl implements MovieService {

	private static final int RECOMMENDATION_MIN_RATING = 8;

	@Autowired
	MovieRepository repository;
	
	@Autowired
	Neo4jTemplate template;
		
	@Override
	@Transactional(readOnly=false)
	public void save(Movie movie, Set<Rating> ratings) {
		if (movie != null) {
			template.save(movie);		
			if (ratings != null) {
				ratings
					.stream()
					.forEach((r) -> {
						template.save(r.getUser());
						r.setMovie(movie);
						template.save(r);
					});		
			}
		}
	}


	@Override
	public Movie findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public List<Movie> findAll() {
		return repository.findAllMovies();
	}

	@Override
	public Page<Movie> findByTitleLike(String title, Pageable pageable) {
		String query = String.format("(?i).*%s.*", title);
		return repository.findByTitleQuery(query, pageable);
	}
	
	@Override
	public Page<Movie> findByTitleStart(String titleStart, Pageable pageable) {
		String query = String.format("(?i)%s.*", titleStart);
		return repository.findByTitleQuery(query, pageable);
	}


	@Override
	public Page<Movie> findByGenre(String genre, Pageable pageable) {
		String query = String.format("(?i)%s", genre);
		return repository.findByGenreQuery(query, pageable);
	}

	@Override
	public List<MovieRecommendation> recommendByFavouriteMovie(Long favouriteMovieId, Integer limit) {
		return repository.recommendByFavouriteMovie(favouriteMovieId, RECOMMENDATION_MIN_RATING,limit);
	}

	@Override
	public List<MovieRecommendation> recommendByMovieRating(Long favouriteMovieId, Integer rating, Integer limit) {
		return repository.recommendByMovieRating(favouriteMovieId, rating, limit);
	}


	@Override
	public List<Movie> findAllByUser(String username) {
		String query = String.format("(?i)%s", username);
		return repository.findAllByUserQuery(query);
	}


	@Override
	public Page<Movie> findByCountry(String country, Pageable pageable) {
		String query = String.format("(?i)%s", country);
		return repository.findByCountry(query, pageable);
	}


}
