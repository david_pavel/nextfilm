package cz.cvut.pavelda2.nextfilm.service;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cz.cvut.pavelda2.nextfilm.domain.Movie;
import cz.cvut.pavelda2.nextfilm.domain.MovieRecommendation;
import cz.cvut.pavelda2.nextfilm.domain.Rating;


public interface MovieService {

	/* ===================== */
	/* === Movies search === */
	/* ===================== */
	
	public Movie findById(Long id);
	public List<Movie> findAll();
	public List<Movie> findAllByUser(String username);
	public Page<Movie> findByTitleLike(String title, Pageable pageable);
	public Page<Movie> findByTitleStart(String titleStart, Pageable pageable);
	public Page<Movie> findByGenre(String genre, Pageable pageable);
	public Page<Movie> findByCountry(String country, Pageable pageable);
	
	/* ======================= */
	/* === Recommendations === */
	/* ======================= */
	public List<MovieRecommendation> recommendByFavouriteMovie(Long favouriteMovieId, Integer limit);
	public List<MovieRecommendation> recommendByMovieRating(Long fdbId, Integer rating, Integer limit);

	/* =========================== */
	/* === Movies modification === */
	/* =========================== */
	public void save(Movie movie, Set<Rating> ratings);
}
