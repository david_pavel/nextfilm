package cz.cvut.pavelda2.nextfilm.config;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.neo4j.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.rest.SpringRestGraphDatabase;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = { "cz.cvut.pavelda2.nextfilm.service" })
@EnableNeo4jRepositories(basePackages = "cz.cvut.pavelda2.nextfilm.repository")
public class Neo4jConfig extends Neo4jConfiguration {

	public Neo4jConfig() {
		setBasePackage("cz.cvut.pavelda2.nextfilm.domain");
	}

	public static final String NEO4J_URL = System.getenv("NEO4J_URL") != null ? System
			.getenv("NEO4J_URL") : "http://localhost:7474/db/data/";
			
	private static final String EMBDEDDED_DATABASE_PATH = "./nextFilm.embedded.db";


	@Bean(destroyMethod = "shutdown", name="graphDatabaseService")
	@Profile("neo4j-rest")
	public GraphDatabaseService restGraphDatabaseService() {
		return new SpringRestGraphDatabase(NEO4J_URL, "neo4j","123");
	}
	
	@Bean(destroyMethod = "shutdown", name="graphDatabaseService")
	@Profile("neo4j-embedded")
	public GraphDatabaseService embeddedGraphDatabaseService() {
		return new GraphDatabaseFactory().newEmbeddedDatabase(EMBDEDDED_DATABASE_PATH);
	}

}