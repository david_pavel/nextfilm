package cz.cvut.pavelda2.nextfilm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import cz.cvut.pavelda2.nextfilm.service.MovieImportService;
import cz.cvut.pavelda2.nextfilm.service.MovieImportServiceEventHandler;
import cz.cvut.pavelda2.nextfilm.service.MovieImportServiceImpl;
import cz.cvut.pavelda2.nextfilm.websocket.MovieImportServiceEventHandlerImpl;

@Configuration
public class CoreAppConfig {
	
	@Autowired
	private SimpMessagingTemplate template;

	@Bean
	public MovieImportService movieImportService() {
		MovieImportService service = new MovieImportServiceImpl();
		service.registerEventHandler(movieImportServiceHandler());
		return service;
	}

	@Bean
	public MovieImportServiceEventHandler movieImportServiceHandler() {
		return new MovieImportServiceEventHandlerImpl(template);		
	}

}
