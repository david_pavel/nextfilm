package cz.cvut.pavelda2.nextfilm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cz.cvut.pavelda2.nextfilm.scraper.fdb.FdbScraper;
import cz.cvut.pavelda2.nextfilm.scraper.fdb.JSoupFdbScraper;

@Configuration
public class ScraperConfig {
	@Bean
	public FdbScraper fdbScraper() {
		return new JSoupFdbScraper();
	}
}
