package cz.cvut.pavelda2.nextfilm.websocket;

import org.springframework.messaging.simp.SimpMessagingTemplate;

import cz.cvut.pavelda2.nextfilm.domain.Movie;
import cz.cvut.pavelda2.nextfilm.service.MovieImportServiceEventHandler;
import cz.cvut.pavelda2.nextfilm.websocket.MovieWSResult.Status;

public class MovieImportServiceEventHandlerImpl implements MovieImportServiceEventHandler {

	private SimpMessagingTemplate template;

	public MovieImportServiceEventHandlerImpl(SimpMessagingTemplate template) {
		super();
		this.template = template;
	}

	@Override
	public void onStart(Long movieId) {
		template.convertAndSend("/monitor/scraping", new MovieWSResult(
				Status.STARTED, movieId));
	}

	@Override
	public void onComplete(Long movieId, Movie movie) {
		template.convertAndSend("/monitor/scraping",
				new MovieWSResult(Status.COMPLETED, movieId, movie.getTitle(),
						movie.getFdbRating()));
	}

	@Override
	public void onException(Long movieId, Throwable throwable) {
		template.convertAndSend("/monitor/scraping",
				new MovieWSResult(Status.EXCEPTION, movieId));
	}

}
