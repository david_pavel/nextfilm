package cz.cvut.pavelda2.nextfilm.websocket;

/**
 * Movie WebSocket result
 * 
 * @author pavelda2
 *
 */
public class MovieWSResult {
	public enum Status {
		STARTED, COMPLETED, EXCEPTION;
	};
	
	private Status status;
	private Long id;
	private String title;
	private Integer rating;
		
	public MovieWSResult() {
		super();
	}
	
	public MovieWSResult(Status status, Long movieId) {
		super();
		this.status = status;
		this.id = movieId;
	}
	
	public MovieWSResult(Status status, Long movieId, String title,
			Integer rating) {
		super();
		this.status = status;
		this.id = movieId;
		this.title = title;
		this.rating = rating;
	}



	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long movieId) {
		this.id = movieId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	@Override
	public String toString() {
		return "MovieWSResult [status=" + status + ", movieId=" + id
				+ ", title=" + title + ", rating=" + rating + "]";
	}
	
	

}
