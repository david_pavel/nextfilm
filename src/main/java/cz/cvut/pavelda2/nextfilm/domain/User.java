package cz.cvut.pavelda2.nextfilm.domain;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.neo4j.helpers.collection.IteratorUtil;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;
import org.springframework.data.neo4j.annotation.RelatedToVia;
import org.springframework.data.neo4j.template.Neo4jOperations;

@NodeEntity
public class User {

	public static final String FRIEND = "FRIEND";
	public static final String RATED_BY = "RATED_BY";

	@GraphId
	protected Long nodeId;
	
	@Indexed(unique = true)
	String username;
	Integer userId;

	@RelatedToVia(type = RATED_BY, direction = Direction.INCOMING)
	@Fetch
	Iterable<Rating> ratings;

	@RelatedTo(type = RATED_BY, direction = Direction.INCOMING)
	Set<Movie> favorites;

	@RelatedTo(type = FRIEND, direction = Direction.BOTH)
	@Fetch
	Set<User> friends;

	public User() {
	}

	public User(String username) {
		this.username = username;
	}

	public void addFriend(User friend) {
		this.friends.add(friend);
	}

	public Rating rate(Neo4jOperations template, Movie movie, int stars,
			String comment) {
		final Rating rating = template.createRelationshipBetween(this, movie,
				Rating.class, RATED_BY, false).rate(stars, comment);
		return template.save(rating);
	}

	public Collection<Rating> getRatings() {
		if (ratings == null) {
			return Collections.emptyList();
		}
		return IteratorUtil.asCollection(ratings);
	}

	
	public Set<User> getFriends() {
		return friends;
	}

	public String getUsername() {
		return username;
	}
	
	
	public Set<Movie> getFavorites() {
		return favorites;
	}

	public void setFavorites(Set<Movie> favorites) {
		this.favorites = favorites;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setRatings(Iterable<Rating> ratings) {
		this.ratings = ratings;
	}

	public void setFriends(Set<User> friends) {
		this.friends = friends;
	}
	
	
	@Override
	public String toString() {
		return "User [nodeId=" + nodeId + ", username=" + username
				+ ", userId=" + userId + ", ratings=" + ratings
				+ ", favorites=" + favorites + ", friends=" + friends + "]";
	}

	public Long getNodeId() {
		return nodeId;
	}

	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public boolean isFriend(User other) {
		return other != null && getFriends().contains(other);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

}