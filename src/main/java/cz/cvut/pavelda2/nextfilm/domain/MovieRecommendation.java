package cz.cvut.pavelda2.nextfilm.domain;

import org.springframework.data.neo4j.annotation.QueryResult;
import org.springframework.data.neo4j.annotation.ResultColumn;

@QueryResult
public class MovieRecommendation {
	@ResultColumn("movie")
	protected Movie movie;
	@ResultColumn("avg")
	protected Double average;
	@ResultColumn("count")
	protected Integer recommendationsCount;
	@ResultColumn("normAvg")
	protected Double normalizedAverage;
	
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	public Double getAverage() {
		return average;
	}
	public void setAverage(Double average) {
		this.average = average;
	}
	public Integer getRecommendationsCount() {
		return recommendationsCount;
	}
	public void setRecommendationsCount(Integer recommendationsCount) {
		this.recommendationsCount = recommendationsCount;
	}
	public Double getNormalizedAverage() {
		return normalizedAverage;
	}
	public void setNormalizedAverage(Double normalizedAverage) {
		this.normalizedAverage = normalizedAverage;
	}
	@Override
	public String toString() {
		return "MovieRecommendation [movie=[id=" + movie.getFdbId() + ", title=" + movie.getTitle() 
				+ "]], average=" + average
				+ ", recommendationsCount=" + recommendationsCount
				+ ", normalizedAverage=" + normalizedAverage + "]";
	}
}
