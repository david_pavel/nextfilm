package cz.cvut.pavelda2.nextfilm.domain;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

@RelationshipEntity(type="RATED_BY")
public class Rating {
    private static final int MAX_STARS = 10;
    private static final int MIN_STARS = 0;

	@GraphId
	protected Long id;
    
    @StartNode
    protected User user;
    @EndNode
    protected Movie movie;    
    protected Integer stars;
    protected String comment;

    public Rating() {
		super();
	}

	public Rating(User user, Movie movie, int stars) {
		this.user = user;
		this.movie = movie;
		this.stars = stars;
	}

	public User getUser() {
        return user;
    }

    public Movie getMovie() {
        return movie;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }
    

    public void setUser(User user) {
		this.user = user;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public void setStars(Integer stars) {
		this.stars = stars;
	}

	public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Rating rate(int stars, String comment) {
        if (stars>= MIN_STARS && stars <= MAX_STARS) this.stars=stars;
        if (comment!=null && !comment.isEmpty()) this.comment = comment;
        return this;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((movie == null) ? 0 : movie.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rating other = (Rating) obj;
		if (movie == null) {
			if (other.movie != null)
				return false;
		} else if (!movie.equals(other.movie))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

    

}
