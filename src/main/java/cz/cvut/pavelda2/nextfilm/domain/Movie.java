package cz.cvut.pavelda2.nextfilm.domain;

import java.util.HashSet;
import java.util.Set;

import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;
import org.springframework.data.neo4j.annotation.RelatedToVia;
import org.springframework.data.neo4j.support.index.IndexType;

@NodeEntity
public class Movie {

	@GraphId
	protected Long id;

	@Indexed(unique = true)
	protected Long fdbId;

	@Indexed(indexType = IndexType.FULLTEXT, indexName = "search")
	protected String title;
	protected Integer year;
    @RelatedTo(type="FILMED_AT_COUNTRY")
	@Fetch
    protected Set<Country> countries;
    @RelatedTo(type="OF_GENRE")
	@Fetch
    protected Set<Genre> genres;
    
	@RelatedToVia(type = "RATED_BY")
	@Fetch
	protected Set<Rating> ratings;
	protected Integer fdbRating;
	protected Integer duration;

	public Movie() {
	}

	public Movie(Long fdbId, String title) {
		this.fdbId = fdbId;
		this.title = title;
	} 
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public void setRatings(Set<Rating> ratings) {
		this.ratings = ratings;
	}

	public int getAvgStars() {
		Iterable<Rating> allRatings = ratings;

		if (allRatings == null)
			return 0;
		int stars = 0, count = 0;
		for (Rating rating : allRatings) {
			stars += rating.getStars();
			count++;
		}
		return count == 0 ? 0 : stars / count;
	}


	public Set<Rating> getRatings() {
		return ratings;
	}

	public Long getFdbId() {
		return fdbId;
	}

	public void setFdbId(Long fdbId) {
		this.fdbId = fdbId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fdbId == null) ? 0 : fdbId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movie other = (Movie) obj;
		if (fdbId == null) {
			if (other.fdbId != null)
				return false;
		} else if (!fdbId.equals(other.fdbId))
			return false;
		return true;
	}
	

	public Set<Country> getCountries() {
		return countries;
	}

	public void setCountries(Set<Country> countries) {
		this.countries = countries;
	}

	public Set<Genre> getGenres() {
		return genres;
	}

	public void setGenres(Set<Genre> genres) {
		this.genres = genres;
	}

	public Integer getFdbRating() {
		return fdbRating;
	}

	public void setFdbRating(Integer fdbRating) {
		this.fdbRating = fdbRating;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	

	public Long getNodeId() {
		return id;
	}

	public void setNodeId(Long nodeId) {
		this.id = nodeId;
	}

	@Override
	public String toString() {
		return "Movie [fdbId=" + fdbId + ", title=" + title + ", year=" + year
				+ ", fdbRating=" + fdbRating + "]";
	}

	public void rate(User user, Movie movie, int stars) {
		if (ratings == null) {
			ratings = new HashSet<Rating>();
		}
		ratings.add(new Rating(user, movie, stars));
	}

}
