package cz.cvut.pavelda2.nextfilm.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cz.cvut.pavelda2.nextfilm.config.Neo4jConfig;
import cz.cvut.pavelda2.nextfilm.domain.Movie;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={Neo4jConfig.class})
@TransactionConfiguration(defaultRollback=true)
@Transactional
@ActiveProfiles("neo4j-embedded")
public class MovieServiceTest {
	
	private MovieService service;
	
	final private Pageable SIMPLE_PAGEABLE = new PageRequest(0, 100);
	
	@Autowired
	public void setMovieService(MovieService service) {
		this.service = service;
	}
	
	@Test
	public void findById() {
		final Long fdbId = 100L;
		final Movie movie = service.findById(fdbId);

		assertNotNull(movie);
		assertEquals(fdbId, movie.getFdbId());
		assertEquals("Forrest Gump", movie.getTitle());
	}
	
	@Test
	public void findAllTest() {
		final List<Movie> movies = service.findAll();

		assertNotNull(movies);
		assertThat(movies.size(), Matchers.greaterThan(0));
		System.out.println(movies);
	}

	@Test
	public void findByTitleLikeTest() {
		final Page<Movie> movies = service.findByTitleLike("orre", SIMPLE_PAGEABLE);

		assertNotNull(movies);
		assertEquals(1,movies.getContent().size());
		assertEquals("Forrest Gump", movies.getContent().get(0).getTitle());
	}
	
	@Test
	public void findByTitleLikeCaseInsensitiveTest() {
		final Page<Movie> movies = service.findByTitleLike("OrRE",SIMPLE_PAGEABLE);

		assertNotNull(movies);
		assertEquals(1,movies.getContent().size());
		assertEquals("Forrest Gump", movies.getContent().get(0).getTitle());
	}



	@Test
	public void findStartWithFTest() {
		final Page<Movie> movies = service.findByTitleStart("F",SIMPLE_PAGEABLE);

		assertNotNull(movies);
		assertEquals(1,movies.getContent().size());
		assertEquals("Forrest Gump", movies.getContent().get(0).getTitle());
	}

	@Test
	public void findStartWithFLowerCaseTest() {
		final Page<Movie> movies = service.findByTitleStart("f",SIMPLE_PAGEABLE);

		assertNotNull(movies);
		assertEquals(1,movies.getContent().size());
		assertEquals("Forrest Gump", movies.getContent().get(0).getTitle());
	}


	@Test
	public void findStartWithXTest() {
		final Page<Movie> movies = service.findByTitleStart("X",SIMPLE_PAGEABLE);

		assertNotNull(movies);
		assertEquals(0,movies.getContent().size());
	}

	
	
	
			
}
