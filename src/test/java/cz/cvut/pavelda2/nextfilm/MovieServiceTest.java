package cz.cvut.pavelda2.nextfilm;

import java.io.File;
import java.util.Set;
import java.util.stream.IntStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cz.cvut.pavelda2.nextfilm.config.Neo4jConfig;
import cz.cvut.pavelda2.nextfilm.config.ScraperConfig;
import cz.cvut.pavelda2.nextfilm.domain.Movie;
import cz.cvut.pavelda2.nextfilm.domain.Rating;
import cz.cvut.pavelda2.nextfilm.scraper.exception.ScrapingException;
import cz.cvut.pavelda2.nextfilm.scraper.fdb.FdbScraper;
import cz.cvut.pavelda2.nextfilm.service.MovieService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Neo4jConfig.class, ScraperConfig.class})
@TransactionConfiguration(defaultRollback = false)
@Transactional
@ActiveProfiles("neo4j-embedded")
public class MovieServiceTest {

	private static final String DATA_PATH = "c:\\Users\\pavelda2\\Workspace\\CVUT\\FIT\\magister\\semestr_2\\MI-DDW\\Homeworks\\hw2\\"
			+ "fdb\\data\\";

	@Autowired
	MovieService service;

	@Autowired
	FdbScraper scraper;

	@Test
	//@Ignore
	public void saveMovies() throws ScrapingException {
		final int start =11000;//43299;
		final int end = 11100;//43300;
		IntStream.range(start, end + 1).parallel().forEach((i) -> {
			//System.out.println("["+i+"] processing movie...");
			File f = new File(DATA_PATH, i + ".html");
			try {
				Movie movie = scraper.retrieveMovie(f);
				Set<Rating> ratings = scraper.retireveRatings(f);
				service.save(movie, ratings);
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("[" + i + "] " + e.getMessage());
			} finally {
				System.out.println("["+i+"] movie processed.");
			}
		});
	}

}
