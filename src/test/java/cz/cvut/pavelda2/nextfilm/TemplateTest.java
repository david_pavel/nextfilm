package cz.cvut.pavelda2.nextfilm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cz.cvut.pavelda2.nextfilm.config.Neo4jConfig;
import cz.cvut.pavelda2.nextfilm.config.ScraperConfig;
import cz.cvut.pavelda2.nextfilm.domain.Movie;
import cz.cvut.pavelda2.nextfilm.domain.Rating;
import cz.cvut.pavelda2.nextfilm.domain.User;
import cz.cvut.pavelda2.nextfilm.scraper.exception.ScrapingException;
import cz.cvut.pavelda2.nextfilm.scraper.fdb.FdbScraper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={Neo4jConfig.class, ScraperConfig.class})
@TransactionConfiguration(defaultRollback=true)
@Transactional
@ActiveProfiles("neo4j-embedded")
public class TemplateTest {
	@Autowired
	protected Neo4jTemplate template;
	
	@Autowired
	private FdbScraper scraper;
	
	@Test
	@Ignore
	public void save() {
		Movie forrest = new Movie(1L, "Forrest Gump");
		template.save(forrest);

		User tommy = new User("tommy");
		template.save(tommy);
		Rating tommyRating = new Rating(tommy, forrest, 8);
		template.save(tommyRating);
		
		User jean = new User("jean");
		template.save(jean);
		Rating jeanRating = new Rating(jean, forrest, 4);
		template.save(jeanRating);
		
		
		Movie movie = template.findByIndexedValue(Movie.class, "fdbId", 1L).single();		
		assertNotNull(movie);
		assertEquals("Forrest Gump", movie.getTitle());
		assertNotNull(movie.getRatings());
		assertEquals(2, movie.getRatings().size());
		assertEquals(8, movie.getRatings().iterator().next().getStars());
		assertEquals(4, movie.getRatings().iterator().next().getStars());
		assertEquals(6, movie.getAvgStars());
	}
	
	@Test
	@Ignore
	public void saveScrapedMovie() throws ScrapingException {
		final File f = new File("c:\\Users\\pavelda2\\Workspace\\CVUT\\FIT\\magister\\semestr_2\\MI-DDW\\Homeworks\\hw2\\"
				+ "fdb\\data\\11000.html");
		Movie film = scraper.retrieveMovie(f);
		template.save(film);
		System.out.println(film);
	}
	

	@Test
	@Ignore
	public void movieWithRatingsSave() throws ScrapingException {
		final File f = new File("c:\\Users\\pavelda2\\Workspace\\CVUT\\FIT\\magister\\semestr_2\\MI-DDW\\Homeworks\\hw2\\"
				+ "fdb\\data\\11005.html");
		Movie movie = scraper.retrieveMovie(f);
		if (movie != null) {
			template.save(movie);
			Set<Rating> ratings = scraper.retireveRatings(f);
			
			if (ratings != null) {
				ratings
					.parallelStream()
					.forEach((r) -> {
						template.save(r.getUser());
						r.setMovie(movie);
						template.save(r);
					});		
			}
		}
	}
	
	@Test
	public void findHighestFdbId() {
		System.out.println("HIGHEST FDBID");
		 Movie movie = template.query("match (m:Movie:_Movie) return m order by m.fdbId desc limit 1", null)
			.to(Movie.class)
			.single();
		 System.out.println(movie);
	}
	
	
	
	
		
}
