package cz.cvut.pavelda2.nextfilm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import cz.cvut.pavelda2.nextfilm.config.Neo4jConfig;
import cz.cvut.pavelda2.nextfilm.domain.Movie;
import cz.cvut.pavelda2.nextfilm.repository.MovieRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Neo4jConfig.class)
@TransactionConfiguration(defaultRollback = true)
@Transactional
@ActiveProfiles("neo4j-embedded")
public class MovieRepositoryTest {
	@Autowired
	MovieRepository repository;
	
	private Pageable simplePageRequest = new PageRequest(0, 5);

	@Test
	public void saveMovie() {
		final String movieTitle = "Forrest Gump";
		final Movie movie = new Movie(100L, movieTitle);
		Movie savedMovie = repository.save(movie);

		assertNotNull(savedMovie);
		assertEquals(movieTitle, savedMovie.getTitle());
	}

	@Test
	public void findMovie() {
		final Long movieId = 11583L;
		final String movieTitle = "Lotrando a Zubejda";
		Movie movie = repository.findById(movieId);

		System.out.println(movie);
		assertNotNull(movie);
		assertEquals(movieTitle, movie.getTitle());
	}
	
	@Test
	public void findByTitleLike() {
		final String movieTitle = "Schindlerův seznam";
		final String movieTitleLike = "seznam";
		Page<Movie> movies = repository.findByTitleQuery(movieTitleLike, simplePageRequest );

		System.out.println(movies.getContent());
		assertNotNull(movies);
		assertEquals(1, movies.getNumberOfElements());
		assertEquals(movieTitle, movies.getContent().get(0).getTitle());
	}
/*
	@Test
	public void recommendMoviesPage() {
		final String movieTitle = "Lotrando a Zubejda";
		final Integer minRating = 7;
		final Movie movie = repository.findByTitle(movieTitle);
		assertNotNull(movie);
		assertNotNull(movie.getFdbId());
		System.out.println("FDB ID: " + movie.getFdbId());
		System.out.println("PAGE: " + simplePageRequest);
		
		Page<MovieRecommendation> recomendations = repository.recommendByMovie(movie.getFdbId(), minRating, simplePageRequest);
		
		assertNotNull(recomendations);
		assertThat(recomendations.getNumberOfElements(), Matchers.greaterThan(0));
		
	}
	
	
	
*/
}
