package cz.cvut.pavelda2.nextfilm.scraper;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.util.StopWatch;

import cz.cvut.pavelda2.nextfilm.domain.Movie;
import cz.cvut.pavelda2.nextfilm.scraper.exception.ScrapingException;
import cz.cvut.pavelda2.nextfilm.scraper.fdb.FdbScraper;
import cz.cvut.pavelda2.nextfilm.scraper.fdb.JSoupFdbScraper;

public class JsoupFdbScraperTest {

	private static StopWatch sw;

	private static FdbScraper scraper;
	
	@BeforeClass
	public static void beforeClass() {
		sw = new StopWatch("Scraper test");
		scraper = new JSoupFdbScraper();
	}
	
	@AfterClass
	public static void afterClass() {		
		System.out.println(sw.prettyPrint());
	}
	
	@Test
	public void filmTest() throws ScrapingException {
		final File f = new File("c:\\Users\\pavelda2\\Workspace\\CVUT\\FIT\\magister\\semestr_2\\MI-DDW\\Homeworks\\hw2\\"
				+ "fdb\\data\\48400.html");
		Movie film = null;
		try {
			sw.start("Film test");
			film = scraper.retrieveMovie(f);
		} finally {
			sw.stop();
		}
		System.out.println(film);
	}
	
	
}
